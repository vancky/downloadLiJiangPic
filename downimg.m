clear ;
%clc;
% 读数据
str=urlread('http://116.55.97.224:9004/gmg_js.php');
%% 设置下载日期
downdate='2015-08-12-';
% 设置时间范围，二十四小时制，只支持小时
bound=[0  5];% 上半夜，[0,5]，白天[6 19]，下半夜[19 24]
% 设置文件名的前半部分
header='http://116.55.97.224:9004//';
% 设置存储位置
filepath='E:\天文学文档\lijiangpics\';
% 找图片名称
res=regexp(str,strcat(downdate,'..-..-..\.jpg'),'match');
%%
No=0;
for k=1:length(res)
    timecal=eval(res{k}(12:13))+eval(res{k}(15:16))/60;
    if timecal>=bound(1)&& timecal<=bound(2)
         No=No+1;
         fprintf('正在写入%s，这是第%d张\n',res{k},No);
         imwrite(imread(strcat(header,res{k}),'jpg'),strcat(filepath,res{k}),'jpg');
    end
end
 fprintf('写入完毕，一共%d张\n',No);